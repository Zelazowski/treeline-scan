## Go to directory with project

Go to directory where you want to create virtual environment

```
cd Projects/MyProject
```

## Create virtual environment

New directory will be created

Method 1:

Virtualenv wrapper

```
virtualenv -p python3 name-of-environment
```

Method 2:

```
python3 -m virtualenv name-of-environment
```

Method 3:

```
python3 -m venv name-of-environment
```


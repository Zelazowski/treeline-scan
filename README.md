# Transects
Python tools for generating topographically consistent sampling transects on raster data.

## Overview
Code in this repository generates topography based-transects at the treeline.  

* A classification into forest/non-forest provides the initial working treeline.  
* Pixels at the upper edge of the forest class serve as the midpoint of transects.   
* Sets of points (transects) are created up- and down-hill for a set number of pixels. Transect direction (one of 8 'octants': *N-S, NE-SW, E-W ... NW-SE*) is determined by the aspect of the pixel that served as it's midpoint.   
* Transect geometries are then used to sample raster datasets.  
* Values are aggregated to transect averages, minima, maxima, etc.  
* If the average aspect angle of all points within the transect places it in another octant, than the one used to determine its orientation, the transect is discarded.  
## Setup
#### Prerequisites
The code in this repo has been created on Ubuntu 20.04, using Python 3.8, and GDAL 3.3.2

#### Python packages
Create virtualenv
```commandline
python3 -m venv venv
source ./venv/bin/activate
```
Install requirements. You may want to change the GDAL version to match the one installed on your system.
```commandline
pip3 install -r requirements.txt
```

#### Testing
You can test if your setup works by calling:
```commandline
python3 -m pytest tests
```


## Usage
After setting the [config values](#0.1) and preparing the [rasters](#0.2) in the directory, the most typical use case: creating data layers and transects for a single directory can be handled using the CLI tool `./bin/generate_scene_transects.py`. The command is as follows:
```commandline
$ python ./bin/generate_scene_transects.py /path/to/data/with/rasters --length 9 --count_offset 100500690000 --gpkg_file /path/to/file.gpkg
```
Where:  

* `/path/to...` - positional argument, path to dir  
* `--length` - number of pixels the transect should contain  
* `--count_offset` - number for indexing transects, might be useful to keep track of which scene a transects come from  
* `--gpkg_file`- path to output file  

What follows is a user-perspective walk through the process, more information can be found in the code in comments.  

### 0. Preparation
#### 0.1. Set config values
Set values in `transects.config`:

* `SKIP_RATE` - the rate at which to skip rows when traversing array - 1: go through entire raster; 20: every 20th row. Set to high number to speed up testing.  
* `DATA_RASTERS` - prefixes or rasters that should end up in transects.  
* `UPPER_EDGE_MIN_ELEV`, `UPPER_EDGE_MAX_ELEV` - bounds for possible treeline elevation, for generating upper class edge working treeline.  

#### 0.2. Prepare input rasters
The process works on a directory at a time, assumed to be e.g. Landsat Path Row dirs.   
All rasters in directory must be 1-band GeoTiffs and adhere to `PREFIX_*.tif` naming, so that columns in the transect dataset can be programmatically named. Scene directories should only contain rasters.  

The transect generation process requires two raster files as input:

* `DEM_*.tif` - [0-8848] a Digital Elevation Model (DEM).  
* `CLASS_*.tif` - [0-1] a binary classification into forest (1) and non-forest (0). Prerequisite for upper edge working tree line and coregistration of all other files.  

**Warning** - keep original datasets elsewhere, as uncoregistered data will be coregistered and the originals- deleted  



Proposed data organization:  
```
scenes/
├─ 004069/
│  ├─ CLASS_LS8-NDVI.tif
│  ├─ DEM_SRTM.tif
│  ├─ OPTIONAL1_lorem-ipsum.tif
│  ├─ OPTIONAL2_lorem-ipsum.tif
├─ 005069/
├─ 006069/
```

### 1. Raster file setup
The `transects.raster_setup.file_set.RasterFileSet` class is used to find within a directory required input and optional data. The class method `RasterFileSet.prepare_set()` coregisters the data and handles creating additional layers necessary for transect creation: 

* `ASPECT_*.tif` - [0-360] terrain aspect values, based on the DEM.    
* `ASPECT-OCT_*.tif` - [0-7] aspect reclassified by 45 deg octant for transect direction (4 straight, 4 diagonal).  
* `CLASS-UPPER-EDGE_*.tif` - [0/1] pixels at the upper edge of the forest class, based on DEM+CLASS.  
For greater ease of insight into the process, the workflows generate and rely on actual files, also for intermediate steps. As such, they may take up a lot of disk space.  

### 2. Creating transects for scene
#### 2.1. Loading rasters into numpy arrays
The `transects.stack.Stack` class loads coregistered rasters into a 3-d array. The following rasters will be used:  
##### Required:

* `CLASS-UPPER-EDGE_*.tif` - for transect direction  
* `ASPECT-OCT_*.tif` - for transect direction  

##### Optional:

* layers in the `transects.config.DATA_RASTERS` config value.   

#### 2.2. Generating transects
The `transects.transects.create_transects` function:  

1. traverses the array searching for transect mid-points (`CLASS-UPPER-EDGE[i,n] == 1`)  
2. generates point transect: point geometries  
3. generates line transect: line geometry  
4. every WRITE_STEP transects:  

   * `rasterstats.point_query` is used to get point values from numpy arrays.  
   * Transect point values are aggregated by transect id to lines.  
   * Transects that contain NaN values or have an average angle that diverges from the orientation are removed  
   * Point and line transects are written to a geopackage   

### 3. Output 
The transects generated are stored in a GeoPackage file, with two internal layers: `points` and `lines`. The `transect_id` column lets you link the two. All geometries are in the original CRS.


You can view the data in QGIS, or read it into a GeoPandas GeoDataFrame:

```python
import geopandas as gpd
lines = gpd.read_file('/path/to/transects.gpkg', layer='lines')
points = gpd.read_file('/path/to/transects.gpkg', layer='points')
```

It is also possible to load the files into a PostGIS database using `ogr2ogr`
```commandline
ogr2ogr -f PostgreSQL PG:"dbname='databasename' host='localhost' port='5432' user='x' password='y'" /path/to/transects.gpkg
```
### 4. After transect creation: stacking and additional input
Additional functions in `transects.post_processing` have been provided for the option to interact with the transect geopackages after generation.


`sample_raster_to_tx` - samples normal raster files (without coregistration)    
`reproject_tx_geopackage` - reprojects to specified EPSG code     
`combine_tx_geopackages` - combines into single files. Be sure to maintain transect_id uniqueness by specifying the `transect_count_offset` when creating the transects.  

## Viz scripts
`viz_scripts/circular.py` - circular plot script

## Author
stefan.jozefowicz@gmail.com
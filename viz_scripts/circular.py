import pathlib

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.font_manager as fm


def bin_deg_values(value_list=np.random.randint(2500,3600,1000), angle_list=np.random.randint(0,360,1000), nbins=24):
    """
    @param value_list: elevation values
    @param angle_list: aspect values
    @param nbins: number of bins (36 - every 10 degs)
    @return: array of average value in bin
    """
    binwidth = 360 / nbins

    circ_correction = []
    for ang in list(angle_list):
        if ang > (360 - binwidth/2):
            circ_correction.append(ang-360)
        else:
            circ_correction.append(ang)

    # bins around the centerpoint
    bins = np.arange(0+binwidth/2, 360+binwidth/2, binwidth)
    arr = np.vstack((np.digitize(circ_correction, bins), value_list)).T

    # change metric here - np.mean, np.max, np.std etc.
    res_lst = [np.mean(arr[arr[:, 0] == i, 1]) for i in range(len(bins))]
    return res_lst

def init_plt_fonts(font:str = "Lato", small_size:int = 10, medium_size:int = 16, large_size:int = 20):
    """Initialize default font family and sizes in matplotlib.pyplot

    @param font: system font name to use
    @param small_size: font size in pt for small captions
    @param medium_size: font size in pt for medium captions
    @param large_size: font size in pt for large captions
    """
    # Define font params usinc rc() function, reference: plt.rcParams 
    plt.rc('font', size=small_size)
    plt.rc(
        'axes', 
        titlesize=medium_size, 
        titlecolor="darkblue", 
        titleweight="bold",
        labelsize=large_size,
        labelweight=900
    )   
    plt.rc('xtick', labelsize=medium_size, labelcolor="silver")
    plt.rc('ytick', labelsize=small_size, labelcolor="dimgray")
    plt.rc('legend', fontsize=medium_size)
    plt.rc('figure', titlesize=large_size)
    # List supported fonts
    sys_fonts = []
    for sys_font in fm.findSystemFonts(fontpaths=None, fontext="ttf"):
        try:
            sys_font_name = fm.get_font(sys_font).family_name
            sys_fonts.append(sys_font)
        except Exception as e:
            print(f"System font: \n\t{sys_font} \nskipped due to following exception: \n\t{e}")
            continue
    if not sys_fonts:
        return
    matches = [match for match in sys_fonts if font.lower() in match.lower()]
    if not matches:
        supported_font_names = [pathlib.Path(x).stem for x in sys_fonts]
        print(f"Selected font {font} not found. Use any of: {supported_font_names}")
        return
    # Apply last match
    fm.fontManager.addfont(matches[-1])
    font_prop = fm.FontProperties(fname=matches[-1])
    # Define font params accessing plt.rcParams directly
    plt.rcParams["font.family"] = font_prop.get_family()[0]
    plt.rcParams["font.sans-serif"] = font_prop.get_name()
    # plt.rcParams["font.weight"] = "bold"


def plot_radial_tl_plot(angle_bin_values, max_value=4500, min_value=2500, label_step=250, figname=None, dpi: int = 200):
    """

    @param angle_bin_values: values for e.g. elevation, ordered from N clockwise
    @return:
    """

    init_plt_fonts()

    # Save directory based on execution file location, not shell working directory 
    if not figname:
        figname = str(pathlib.Path(__file__).resolve().parents[1].joinpath("export.png"))
    
    angle_bins = list(np.arange(0,2*np.pi,2*np.pi / len(angle_bin_values))) # e.g every 36 degs
    fig, ax = plt.subplots(subplot_kw={'projection': 'polar'},figsize=(10,10))

    # make clockwise
    ax.set_theta_direction(-1)
    # make 0 deg = N / top
    ax.set_theta_zero_location("N")

    ax.set_rlim(bottom=max_value, top=min_value)

    ax.set_rticks(np.arange(min_value,max_value,label_step))  # Less radial ticks
    ax.set_rlabel_position(-22.5)  # Move radial labels away from plotted lines


    # plot with duplicated first data point so the plot goes all the way around
    ax.plot(angle_bins+[2*np.pi], angle_bin_values+[angle_bin_values[0]],ls="-",color="g",linewidth=5)

    ax.tick_params(axis='y')

    ax.grid(True)

    # Overwrite global font setting from plt.rcParams
    font_props = {
        'color':  'magenta',
        'weight': 900,
        'size': 30,
    }

    ax.set_title("Elevation vs. aspect", va='bottom', fontdict=font_props)
    plt.savefig(figname, dpi=dpi)
    if pathlib.Path(figname).exists():
        print(f"Saved: {figname}")
    plt.show()
    

if __name__ == "__main__":
    # test
    values=np.random.randint(2500, 3600, 1000)
    angles=np.random.randint(0, 360, 1000)

    # test 357 deg ends up in (-5 - 5) bin
    values[50] = 30000
    angles[50] = 357

    values[60] = -10000
    angles[60] = 45

    # this high value should affect the Northern most bin average
    sample_binned_values = bin_deg_values(values, angles)

    # and be visible in the top of the chart
    plot_radial_tl_plot(sample_binned_values)
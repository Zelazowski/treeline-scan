
from pathlib import Path

import geopandas as gpd



test_sample_gpkg = Path(__file__).resolve().parent.parent.joinpath("tests/test_data/transects_PZ.gpkg")

def get_tx_value_at_last_tree_pixel(transects_points,
                                          tree_class_column_name: str = "GLAD2020",
                                          value_to_be_extracted: str = "DEM",
                                          ):
    """
    For each transect in point dataset, gets last pixel that is 1, extracts specified value
    """
    value_series = transects_points.loc[transects_points[tree_class_column_name] >= 1].sort_values(
        ["pt_id"], ascending=False).groupby(
        "transect_id")["transect_id",value_to_be_extracted].head(1)
    return value_series

def merge_extracted_tx_values_to_lines(gpkg= test_sample_gpkg):

    points = gpd.read_file(gpkg, layer="points")

    lines = gpd.read_file(gpkg, layer="lines")

    extracted_heights = get_tx_value_at_last_tree_pixel(
        transects_points=points,
        tree_class_column_name= "GLAD2020",
        value_to_be_extracted= "DEM",
    )
    lines = lines.merge(extracted_heights, on="transect_id", how="left")
    return lines


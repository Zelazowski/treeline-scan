import argparse
from pathlib import Path

import sys

sys.path.append(str(Path(__file__).parent.parent.resolve()))


from transects.transects import create_transects


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Tool to create transects for given scene directory"
    )
    parser.add_argument("scene_dir", help="Path to directory containing raster files")
    parser.add_argument(
        "--length",
        help="Length of genereted transects, in pixels of main raster source",
        default=21,
    )
    parser.add_argument(
        "--count_offset",
        help="Offset for transect counting, e.g. 100406900000000",
        default=0,
    )
    parser.add_argument("--gpkg_file", help="Path to file for output", default=None)

    args = parser.parse_args()
    scene_dir = Path(args.scene_dir).resolve()
    if not scene_dir.exists():
        raise ValueError(f"Directory {args.scene_dir} does not exist")

    if args.gpkg_file is not None:
        gpkg_file = Path(args.gpkg_file).resolve()
        if not gpkg_file.parent.exists():
            raise ValueError(f"Directory {args.scene_dir} does not exist")
    else:
        gpkg_file = None

    create_transects(dir_path=scene_dir,length=int(args.length),
                     transect_count_start=int(args.count_offset),gpkg_path=gpkg_file)

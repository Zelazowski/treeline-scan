from pathlib import Path

import numpy as np
import rasterio
import tqdm
from osgeo import gdal

from transects.config import UPPER_EDGE_MIN_ELEV, UPPER_EDGE_MAX_ELEV
from transects.raster_angles import angles_to_octants


def file_upper_edge(class_tif: Path, dem_tif: Path, upperedge_tif: Path):
    with rasterio.open(class_tif) as src:
        class_array = src.read(1)
        class_profile = src.profile

    with rasterio.open(dem_tif) as src:
        dem_array = src.read(1)
    print(f"Finding upper edge of forest class")
    upper_edge_array = upper_edge(class_array, dem_array)

    with rasterio.open(upperedge_tif, "w", **class_profile) as dst:
        dst.write(upper_edge_array, 1)


def upper_edge(
    class_array: np.array,
    dem_array: np.array,
    min_elev: float = UPPER_EDGE_MIN_ELEV,
    max_elev: float = UPPER_EDGE_MAX_ELEV,
) -> np.array:
    shape = class_array.shape
    upper_edge_array = np.zeros(shape)
    class_array[dem_array > max_elev] = 0
    class_array[dem_array < min_elev] = 0
    for x in tqdm.trange(1, shape[0] - 1):
        for y in range(1, shape[1] - 1):
            # check if pixel is potential upper edge of forest - forest
            if class_array[x, y] == 1:
                nonforest_true_mask = class_array[x - 1 : x + 2, y - 1 : y + 2] == 0
                # check if any in mask is nonforest
                if np.any(nonforest_true_mask):
                    dem_mask = dem_array[x - 1 : x + 2, y - 1 : y + 2]
                    nonforest_dem = nonforest_true_mask * dem_mask
                    forest_dem = np.invert(nonforest_true_mask) * dem_mask
                    # check if highest neighbor is nonforest
                    if np.max(nonforest_dem) > np.max(forest_dem):
                        upper_edge_array[x, y] = 1
    return upper_edge_array


def file_gdal_aspect(DEM: str, aspect_out: str):
    gdal.DEMProcessing(aspect_out, DEM, "aspect")


def file_gdal_slope(DEM: str, slope_out: str):
    gdal.DEMProcessing(slope_out, DEM, "slope")


def file_aspect_octant(aspect_tif: str, aspect_octant_tif: str):
    with rasterio.open(aspect_tif) as src:
        array = src.read(1)
        profile = src.profile
    rec_array = angles_to_octants(array)
    with rasterio.open(aspect_octant_tif, "w", **profile) as dst:
        dst.write(rec_array, 1)

import os
from pathlib import Path
from pathlib import _PosixFlavour, _WindowsFlavour

from transects.raster_setup.dem_process import (
    file_gdal_aspect,
    file_aspect_octant,
    file_upper_edge,
)
from transects.raster_setup.raster_utils import coregister, are_coregistered

"""
Create test_data necessary for transect generation

For greater ease of insight along the process, the workflows will rely on actual files, and most processing 
steps will generate a new dataset. As such, it may take up a lot of disk space.

Warning - keep original files elsewhere, as uncoregistered test_data will be coregistered and the originals- deleted

Directory where we want to prepare test_data for stacking and subsequent use for transect generation should only
contain single band geotiff test_data layers.

Required present input layers:
    CLASS_*.tif - classification to be used as anchor for working treeline, and basis for coregistration of all files.
                  Must be in UTM crs. 
                  All other layers must have the same or greater spatial extent to avoid NODATA issues
                  
    DEM_*.tif - terrain model for generating topography based layers. If not already coregistered to CLASS_, 
                    it will be done automatically using the Bilinear resampling method. Do this manually if you 
                    want to use another method (use the raster_setup.coregister.coregister function) or change manually
                    
Automatically generated:
    Needed for transect generation
    ASPECT_*.tif - [0-360]
    ASPECT-OCT_*.tif - [0-8]
    CLASS-UPPER-EDGE_*.tif - [0-1]
    
Optional additional layers:
    WHATEVER-WORKS_*.tif  - E.g., climate test_data, LS8 bands. If not already registered to CLASS_, 
                            it will be done automatically using the Bilinear resampling method. Do this manually if you 
                            want to use another method (use the raster_setup.coregister.coregister function) or change manually
"""


# https://pythonhosted.org/rasterstats/manual.html#raster-data-sources probably doesn't make sense to do the raster
# coregistration thing beyond creating the transects themselves, then ready made geom rasterstats


class RasterFileSet:
    def __init__(self, rasterdirpath: str):
        self.dirpath = Path(rasterdirpath)
        self._find_class()
        self._find_dem()

    def prepare_set(self, coregister: bool = True, dem_layers: bool = True):
        if coregister:
            # coregister inplace all test_data to class_raster
            self._coregister_set()

        if dem_layers:
            # create DEM-derived layers
            self._generate_dem_layers()

    def get_raster_dict(self):
        rasters = {}
        for f in self.dirpath.glob("*.tif"):
            raster = Raster(RasterPath(f))
            rasters[raster.name] = raster

        self.raster_dict = rasters
        return rasters

    def _generate_dem_layers(self):
        # Aspect
        aspect = Raster(self.dem.path.swap_prefix("ASPECT"), self.classif)
        if not aspect.path.exists():
            file_gdal_aspect(str(self.dem.path), str(aspect.path))

        # Reclassed aspect
        aspect_rec = Raster(aspect.path.swap_prefix("ASPECT-OCT"), self.classif)
        if not aspect_rec.path.exists():
            file_aspect_octant(aspect.path, aspect_rec.path)

        # Create upper edge pixels
        class_upper_edge = Raster(
            self.classif.path.swap_prefix("CLASS-UPPER-EDGE"), self.classif
        )
        if not class_upper_edge.path.exists():
            file_upper_edge(self.classif.path, self.dem.path, class_upper_edge.path)

    def _coregister_set(self):
        self.get_raster_dict()
        for raster in list(self.raster_dict.values()) + [self.dem]:
            raster.coregister()
        self._check_all_coregistered()

    def _find_class(self):
        self.classif = Raster(
            RasterPath(list(self.dirpath.glob("CLASS*.tif"))[0]), None
        )

    def _find_dem(self):
        self.dem = Raster(
            RasterPath(list(self.dirpath.glob("DEM*.tif"))[0]), self.classif
        )

    def _check_all_coregistered(self):
        self.get_raster_dict()
        return all(
            [f.is_coregistered() for f in list(self.raster_dict.values()) + [self.dem]]
        )


class Raster:
    def __init__(self, path, primary_raster=None):
        self.path = RasterPath(path)
        self.primary_raster = primary_raster
        self.name = self.path.prefix

    def is_coregistered(self):
        if self.primary_raster is None:
            self.coregistered = True
            return self.coregistered
        elif not self.path.exists():
            self.coregistered = False
            return self.coregistered
            return False
        else:
            self.coregistered = are_coregistered(self.primary_raster.path, self.path)
            return self.coregistered

    def coregister(self):
        if not self.is_coregistered():
            tempname = self.path.parent.joinpath("coreg_temp.tif")
            coregister(self.primary_raster.path, self.path, tempname)
            tempname.replace(self.path)
            self.coregistered = are_coregistered(self.primary_raster.path, self.path)

    def __str__(self):
        return str(self.path)


class RasterPath(Path):
    _flavour = _PosixFlavour() if os.name == "posix" else _WindowsFlavour()

    def __new__(cls, *args):
        instance = super(RasterPath, cls).__new__(cls, *args)
        return instance

    def __init__(self, *args):
        super().__init__()
        self.validate_name()
        self.prefix = self.name.split("_")[0]
        self.basename = self.name.replace(f"{self.prefix}_", "")

    def swap_prefix(self, new_prefix):
        newname = self.parent.joinpath(f"{new_prefix}_{self.basename}")
        return RasterPath(newname)

    def validate_name(self):
        if len(self.name.split("_")) < 2 or self.suffix.lower() != ".tif":
            raise ValueError(
                "Bad raster name - should be tif and follow pattern /path/to/file/PREFIX_rest-of-name.tif"
            )

from pathlib import Path

import rasterio
from rasterio import shutil as rio_shutil
from rasterio.enums import Resampling
from rasterio.vrt import WarpedVRT


def get_raster_array(raster: Path):
    with rasterio.open(raster) as src:
        array = src.read(1)
    return array


def get_raster_profile(raster: Path) -> dict:
    with rasterio.open(raster) as src:
        profile = src.profile
    return profile


def coregister(
    master_raster: Path,
    matched_raster: Path,
    output_matched_raster: Path,
    resampling_method=Resampling.bilinear,
):
    """
    Used to create rasters with matching CRS, point of origin and resolution for use in stacks
    https://rasterio.readthedocs.io/en/latest/topics/virtual-warping.html
    """
    # get info of master raster
    output_params = get_coregistration_dict(master_raster)

    # resample and reproject
    output_params.update(
        {
            "resampling": resampling_method,
        }
    )

    # write to file
    with rasterio.open(matched_raster) as mr:
        with WarpedVRT(mr, **output_params) as vrt:
            data = vrt.read()
            rio_shutil.copy(vrt, output_matched_raster, driver="GTiff")


def are_coregistered(master_raster: Path, checked_raster: Path) -> bool:
    master_coregistration = get_coregistration_dict(master_raster)
    checked_coregistration = get_coregistration_dict(checked_raster)
    coregistered = True
    for key in master_coregistration.keys():
        if checked_coregistration[key] != master_coregistration[key]:
            coregistered = False
    return coregistered


def get_coregistration_dict(raster: Path) -> dict:
    coregistration_dict = {}
    with rasterio.open(raster) as src:
        coregistration_dict["crs"] = src.crs
        coregistration_dict["height"] = src.shape[0]
        coregistration_dict["width"] = src.shape[1]
        coregistration_dict["transform"] = src.transform
    return coregistration_dict

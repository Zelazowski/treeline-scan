from pathlib import Path
from datetime import datetime

import geopandas as gpd
import pandas as pd
import tqdm
from rasterstats import point_query
from scipy.stats import circmean, circstd
from shapely.geometry import LineString, Point


from transects.config import DATA_RASTERS, SKIP_RATE, WRITE_STEP, RASTER_NO_DATA_VALUE, TOLERANCE_ANGLE
from transects.stack import Stack
from transects.raster_angles import angles_to_octants, MetaTransect


def create_transects(
    dir_path: str,
    transect_count_start: int,  # =1004069000000
    length: int,
    gpkg_path: Path= None,
    skip_rate: int = SKIP_RATE,
):
    if gpkg_path is None:
        gpkg_path = dir_path.joinpath("transects.gpkg")
    if gpkg_path.exists():
        gpkg_path.unlink()
    if length % 2 == 0:
        raise ValueError("length parameter has to be odd integer")

    stack = Stack(dir_path)

    metatransect = MetaTransect(stack.transform[0], length)

    # operations on pandas / geopandas objects are slow, only use during write / analytics
    tx_points_dict = {
        "transect_id": [],
        "pt_id": [],
        "geometry": [],
    }
    tx_lines_dict = {
        "transect_id": [],
        "orientation": [],
        "geometry": [],
    }



    transect_count = 0

    # y, lat, increase in i >> decrease in y
    print(f"{datetime.now().isoformat()} Generating transects")
    for i in tqdm.trange(0, stack.array.shape[1], skip_rate):
        # x, lon, increase in n >> increase in x
        for n in range(stack.array.shape[2]):
            # if pixel is working tree line and has octant aspect value
            if stack.array[0, i, n] == 1 and stack.array[1, i, n] != -9999:
                transect_id = transect_count + transect_count_start
                octant = stack.array[1, i, n]

                tx_points, tx_line = create_features(
                                                stack.transform,
                                                octant,
                                                (i, n),
                                                transect_id,
                                                metatransect
                )
                for key in tx_points_dict.keys():
                    tx_points_dict[key] += tx_points[key]
                for key in tx_lines_dict.keys():
                    tx_lines_dict[key] += tx_line[key]


                # Get raster test_data into geodataframes, write to file and wipe dataframes every WRITE_STEP transects
                if (transect_count + 1) % WRITE_STEP == 0:
                    tx_points_gdf = gpd.GeoDataFrame(tx_points_dict).set_crs(f"EPSG:{stack.crs}")
                    tx_lines_gdf = gpd.GeoDataFrame(tx_lines_dict).set_crs(f"EPSG:{stack.crs}")
                    tx_points_dict = {
                        "transect_id": [],
                        "pt_id": [],
                        "geometry": [],
                    }
                    tx_lines_dict = {
                        "transect_id": [],
                        "orientation": [],
                        "geometry": [],
                    }
                    get_tx_point_data(tx_points_gdf, stack)

                    tx_points_gdf, tx_lines_gdf = aggregate_transects(
                                                    tx_points_gdf,
                                                    tx_lines_gdf,
                    )
                    tx_points_gdf, tx_lines_gdf = prune_transects(
                                                    tx_points_gdf,
                                                    tx_lines_gdf,
                    )

                    tx_points_gdf, tx_lines_gdf = write_to_geopackage_and_wipe(
                                                    tx_points_gdf,
                                                    tx_lines_gdf,
                                                    gpkg_file=gpkg_path,
                                                    crs=stack.crs,
                    )

                transect_count += 1

    # Get raster test_data into geodataframes, write to file and wipe dataframes for remainder
    tx_points_gdf = gpd.GeoDataFrame(tx_points_dict).set_crs(f"EPSG:{stack.crs}")
    tx_lines_gdf = gpd.GeoDataFrame(tx_lines_dict).set_crs(f"EPSG:{stack.crs}")
    get_tx_point_data(tx_points_gdf, stack)

    tx_points_gdf, tx_lines_gdf = aggregate_transects(
        tx_points_gdf,
        tx_lines_gdf,
    )
    tx_points_gdf, tx_lines_gdf = prune_transects(
        tx_points_gdf,
        tx_lines_gdf,
    )
    tx_points_gdf, tx_lines_gdf = write_to_geopackage_and_wipe(
        tx_points_gdf,
        tx_lines_gdf,
        gpkg_file=gpkg_path,
        crs=stack.crs,
    )
    print(f"{datetime.now().isoformat()} Done")

    return tx_points_gdf, tx_lines_gdf


def aggregate_transects(tx_points_gdf, tx_lines_gdf):
    # aggregate to transects
    # aspect values
    tx_lines_gdf = aggregate_aspect_values_to_tx(tx_lines_gdf, tx_points_gdf)

    # other values
    tx_lines_gdf = aggregate_data_raster_values_to_tx(tx_lines_gdf, tx_points_gdf)
    return tx_points_gdf, tx_lines_gdf

def prune_transects(tx_points_gdf, tx_lines_gdf):
    # remove tx with avg aspect differing from orientation
    tx_lines_gdf = remove_topo_inconsistent_tx(tx_lines_gdf)
    # gets rid of transects where NaNs show up in point test_data

    tx_lines_gdf.dropna(inplace=True)
    tx_points_gdf = tx_points_gdf.loc[
        tx_points_gdf["transect_id"].isin(tx_lines_gdf["transect_id"])
    ]

    return tx_points_gdf, tx_lines_gdf


def write_to_geopackage_and_wipe(
    tx_points_gdf: gpd.GeoDataFrame,
    tx_lines_gdf: gpd.GeoDataFrame,
    gpkg_file: Path,
    crs=32618,
):
    if not gpkg_file.exists():
        mode = "w"
    else:
        mode = "a"
    tx_points_gdf.to_file(gpkg_file, layer="points", driver="GPKG", mode=mode)
    tx_lines_gdf.to_file(gpkg_file, layer="lines", driver="GPKG", mode=mode)

    tx_lines_gdf = gpd.GeoDataFrame(
        columns=["transect_id", "orientation", "geometry"]
    ).set_crs(f"EPSG:{crs}")
    tx_points_gdf = gpd.GeoDataFrame(
        columns=["transect_id", "pt_id", "geometry"]
    ).set_crs(f"EPSG:{crs}")

    return gpd.GeoDataFrame().reindex(
        columns=tx_points_gdf.columns
    ), gpd.GeoDataFrame().reindex(columns=tx_lines_gdf.columns)


def indices_to_coordinates(i, n, transform):
    y = (i + 0.5) * transform[4] + transform[5]
    x = (n + 0.5) * transform[0] + transform[2]
    return y, x


def create_features(
    transform,
    octant: int,
    array_indices: tuple,
    transect_id: int,
    metatransect: MetaTransect,
):
    i, n = array_indices
    y, x = indices_to_coordinates(i, n, transform)

    transect_points = {
        "transect_id": [],
        "pt_id": [],
        "geometry": [],
    }

    # generate transect points
    for nb, pt in enumerate(
        metatransect.shift_transect(octant, y, x)
    ):
        if nb == 0:
            transect_start = pt
        point = Point(pt)
        transect_points["transect_id"].append(transect_id)
        transect_points["pt_id"].append(nb)
        transect_points["geometry"].append(point)

    # generate line out of first and last transect point
    linestring = LineString([transect_start, pt])
    transect_line = {
        "transect_id": [transect_id],
        "orientation": [octant],
        "geometry": [linestring],
    }
    return transect_points, transect_line



def get_tx_point_data(
    tx_points_gdf: gpd.GeoDataFrame,
    array_stack: Stack,
):
    for dr in DATA_RASTERS:
        get_raster_values(tx_points_gdf, array_stack, dr)


def get_raster_values(
    tx_points_gdf: gpd.GeoDataFrame, array_stack: Stack, raster_prefix: str
):
    tx_points_gdf[raster_prefix] = point_query(
        tx_points_gdf.geometry,
        array_stack.get_dataset_array_by_prefix(raster_prefix),
        affine=array_stack.transform,
        nodata=RASTER_NO_DATA_VALUE,
        interpolate="nearest"
    )


def aggregate_aspect_values_to_tx(
    tx_lines_gdf: gpd.GeoDataFrame, tx_points_gdf: gpd.GeoDataFrame
):
    # Circular test_data - circ mean and circ std instead of min max
    tx_lines_gdf = tx_lines_gdf.merge(
        tx_points_gdf.groupby("transect_id")["ASPECT"].agg(
            circstd, **{"high": 360, "low": 0}
        ),
        on="transect_id",
    )
    tx_lines_gdf = tx_lines_gdf.rename(columns={"ASPECT": "ASPECT_circstd"})

    tx_lines_gdf = tx_lines_gdf.merge(
        tx_points_gdf.groupby("transect_id")["ASPECT"].agg(
            circmean, **{"high": 360, "low": 0}
        ),
        on="transect_id",
    )
    tx_lines_gdf = tx_lines_gdf.rename(columns={"ASPECT": "ASPECT_circavg"})
    return tx_lines_gdf


def aggregate_data_raster_values_to_tx(
    tx_lines_gdf: gpd.GeoDataFrame, tx_points_gdf: gpd.GeoDataFrame
) -> gpd.GeoDataFrame:
    # standard min max avg
    for characteristic in [f for f in DATA_RASTERS if f != "ASPECT"]:
        tx_lines_gdf = aggregate_to_tx(tx_lines_gdf,tx_points_gdf,characteristic)
    return tx_lines_gdf

def aggregate_to_tx(tx_lines_gdf,tx_points_gdf,characteristic):
    tx_lines_gdf = tx_lines_gdf.merge(
        tx_points_gdf.groupby("transect_id").agg(
            **{
                f"{characteristic}_max": pd.NamedAgg(characteristic, "max"),
                f"{characteristic}_min": pd.NamedAgg(characteristic, "min"),
                f"{characteristic}_avg": pd.NamedAgg(characteristic, "mean"),
            }
        ),
        on="transect_id",
    )
    return tx_lines_gdf

def remove_topo_inconsistent_tx(tx_lines_gdf: gpd.GeoDataFrame) -> gpd.GeoDataFrame:
    tx_lines_gdf["orientation_avg"] = angles_to_octants(tx_lines_gdf["ASPECT_circavg"],tolerance=TOLERANCE_ANGLE)
    tx_lines_gdf["consistent"] = (
        tx_lines_gdf["orientation_avg"] == tx_lines_gdf["orientation"]
    )
    tx_lines_gdf = tx_lines_gdf.loc[tx_lines_gdf["consistent"]]
    tx_lines_gdf = tx_lines_gdf.drop(columns=["consistent", "orientation_avg"])
    return tx_lines_gdf

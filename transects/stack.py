from pathlib import Path

import numpy as np

from transects.raster_setup.raster_utils import get_raster_array, get_raster_profile
from transects.raster_setup.file_set import RasterFileSet

from transects.config import TRANSECT_ORIGIN_RASTER, DATA_RASTERS


class Stack:
    datasets = [TRANSECT_ORIGIN_RASTER] + ["ASPECT-OCT", "ASPECT", "DEM"] + DATA_RASTERS

    def __init__(self, dirpath: str):
        self.file_set = RasterFileSet(dirpath)
        self.file_set.prepare_set()
        self.raster_dict = self.file_set.get_raster_dict()
        self.profile = get_raster_profile(self.raster_dict[self.datasets[0]].path)
        self.transform = self.profile["transform"]
        self.crs = self.profile["crs"].to_epsg()
        self.stack_array()

    def stack_array(self):
        arrays = []
        for dp in self.datasets:
            arrays.append(self._read_dataset_into_array(self.raster_dict[dp].path))
        self.array = np.stack(arrays)

    def get_dataset_array_by_prefix(self, prefix: str) -> np.array:
        index = self.datasets.index(prefix)
        return self.array[index, :, :]

    def _read_dataset_into_array(self, rasterpath: Path) -> np.array:
        array = get_raster_array(rasterpath).astype(np.float32)
        return array

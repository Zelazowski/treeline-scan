"""
If you want to use a different raster as transect point of origin (midpoint) - 0 no origin, 1 origing
"""

TRANSECT_ORIGIN_RASTER = "CLASS-UPPER-EDGE"



"""
Data layers - transect generation requires:
                        'ASPECT-OCT', 'CLASS-UPPER-EDGE'
but they are not useful for the analysis itself
specify the DATA_RASTERS layers, that should end up in the transect output, e.g. NDVI, DEM, ASPECT, etc.
"""
DATA_RASTERS = [
    "DEM",  # Must be placed in RasterFileStack directory
    "ASPECT",  # Automatically generated when making CLASS-UPPER-EDGE
    "GLAD2020",
    "CHELSA-tas",
    "CHELSA-tasmin",
    
]

RASTER_NO_DATA_VALUE = -9999

"""
When creating class-upper edge working tree line, you can get rid of part of non-TL class boundaries: 
pixels below and above given threshold will be ignored
"""
UPPER_EDGE_MIN_ELEV = 2500
UPPER_EDGE_MAX_ELEV = 4000


"""
For testing you may want to limit the number of transects generated. 1/SKIP_RATE pixels will be used.
"""
SKIP_RATE = 10


"""
How much can the average aspect angle of the transect deviate from  its orientation angle - 
setting to more than 22.5 doesnt make sense
"""
TOLERANCE_ANGLE = 22.5



"""
Write to file every WRITE_STEP transects
"""
WRITE_STEP = 1000

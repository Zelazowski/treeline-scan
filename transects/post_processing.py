from pathlib import Path
import multiprocessing as mp

import geopandas as gpd
import pandas as pd
import numpy as np
from rasterstats import point_query

from transects.transects import aggregate_to_tx

def reproject_tx_geopackage(tx_geopackage: str, out_tx_geopackage: str=None, out_epsg: int=4326):
    tx_geopackage = Path(tx_geopackage)
    if out_tx_geopackage is None:
        out_tx_geopackage = tx_geopackage.parent.joinpath(f"{tx_geopackage.stem}_{out_epsg}.gpkg")
    else:
        out_tx_geopackage = Path(out_tx_geopackage)

    points = gpd.read_file(tx_geopackage,layer='points')
    lines = gpd.read_file(tx_geopackage, layer='lines')

    points = points.to_crs(f'EPSG:{out_epsg}')
    lines = lines.to_crs(f'EPSG:{out_epsg}')
    if out_tx_geopackage.exists():
        mode='a'
    else:
        mode='w'
    points.to_file(out_tx_geopackage,layer='points',mode=mode)
    lines.to_file(out_tx_geopackage, layer='lines', mode=mode)

def combine_tx_geopackages(tx_geopackages:list,out_tx_geopackage: str):
    out_tx_geopackage = Path(out_tx_geopackage)
    for tx in tx_geopackages:
        if out_tx_geopackage.exists():
            mode = 'a'
        else:
            mode = 'w'

        points = gpd.read_file(tx, layer='points')
        lines = gpd.read_file(tx, layer='lines')
        points.to_file(out_tx_geopackage, layer='points', mode=mode)
        lines.to_file(out_tx_geopackage, layer='lines', mode=mode)


def sample_raster_to_tx(tx_geopackage:str,out_tx_geopackage:str,raster_path:str,characteristic_name:str, proc_no:int=mp.cpu_count()-2):
    points = gpd.read_file(tx_geopackage,layer='points')
    lines = gpd.read_file(tx_geopackage, layer='lines')

    out_tx_geopackage = Path(out_tx_geopackage)
    print(f"Sampling raster {raster_path} to transect points using {proc_no} CPUs...")
    points_split_list = np.array_split(points, proc_no)
    starmap_params = [(f,characteristic_name,raster_path) for f in points_split_list]
    with mp.Pool(proc_no) as pool:
        results=  pool.starmap(sample_raster_points,starmap_params)

    points = pd.concat(results)

    print("Sampling finished. Aggregating to transect lines")
    lines = aggregate_to_tx(tx_lines_gdf=lines,tx_points_gdf=points,characteristic=characteristic_name)
    if out_tx_geopackage.exists():
        mode = 'a'
    else:
        mode = 'w'

    points.to_file(out_tx_geopackage, layer='points', mode=mode)
    lines.to_file(out_tx_geopackage, layer='lines', mode=mode)

def sample_raster_points(points,characteristic_name,raster_path):
    points[characteristic_name] = point_query(points, raster_path)
    return points

def parallelize_dataframe(df, func, n_cores=4):
    df_split = np.array_split(df, n_cores)
    pool = mp.Pool(n_cores)
    df = pd.concat(pool.map(func, df_split))
    pool.close()
    pool.join()
    return df



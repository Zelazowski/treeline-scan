import numpy as np
from datetime import datetime


class MetaTransect:
    """
    Transect combinations for a given raster transform/resolution and transect length
    """
    def __init__(self, pixel_width: float, length: int):
        self.pixel_width = pixel_width
        self.length = length
        increasing_y = increasing_ind(0, length, pixel_width)
        increasing_x = increasing_ind(0, length, pixel_width)
        decreasing_y = decreasing_ind(0, length, pixel_width)
        decreasing_x = decreasing_ind(0, length, pixel_width)

        self.climb_coordinates = {
                0: (np.array(length * [0]), decreasing_y),  # N  .: S
                1: (decreasing_x, decreasing_y),  # NE .: SW
                2: (decreasing_x, np.array(length * [0])),  # E  .: W
                3: (decreasing_x, increasing_y),  # SE .: NW
                4: (np.array(length * [0]), increasing_y),  # S  .: N
                5: (increasing_x, increasing_y),  # SW .: NE
                6: (increasing_x, np.array(length * [0])),  # W  .: E
                7: (increasing_x, decreasing_y),  # NW .: SE
        }

    def shift_transect(self, octant: int, y: float, x: float):
        """returns coordinates of a raster with a given orientation at particular coordinates"""
        return zip(self.climb_coordinates[octant][0] + x, self.climb_coordinates[octant][1] + y)



def angles_to_octants(array: np.array, tolerance=22.5) -> np.array:
    centers = np.arange(0, 405, 45)
    lows = centers - tolerance
    highs = centers + tolerance
    out_array = np.full(shape=array.shape, fill_value=-9999)
    for i, (low, high) in enumerate(zip(lows, highs)):
        out_array[(low < array) & (array <= high)] = i
    out_array[out_array == 8] = 0
    return out_array



def increasing_ind(z: float, length: int, pixel_width: float):
    indlist = np.arange(
            (length - 1) * -0.5 * pixel_width + z,
            (length - 1) * 0.5 * pixel_width + z + pixel_width,
            pixel_width,
        )

    return indlist


def decreasing_ind(z: float, length: int, pixel_width: float):
    indlist = np.arange(
            ((length - 1) * 0.5 * pixel_width + z),
            ((length - 1) * -0.5 * pixel_width) + z - pixel_width,
            -pixel_width,
        )
    return indlist


def transect_distance(octant: int, pixel_width: float):
    if octant % 2 == 0:  # straight N-S, W-E
        return pixel_width
    else:  # diagonals NW-SE
        return pixel_width * 1.41421

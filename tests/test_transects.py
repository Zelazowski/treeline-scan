import sys
from pathlib import Path
sys.path.append(str(Path(__file__).parent.parent.resolve()))

import geopandas as gpd

from conftest import sample_dir
from transects.transects import create_transects
from transects.raster_setup.raster_utils import are_coregistered



def test_end_to_end_transect_generation(remove_test_data_dir,create_test_data_dir):
    create_transects(dir_path=sample_dir,length=9,transect_count_start=10000,skip_rate=400)

    # check if coregistration works
    assert are_coregistered(sample_dir.joinpath('DEM_merged.tif'),
                            sample_dir.joinpath('CLASS-UPPER-EDGE_LS8-NDVI_clipped.tif'))

    # check if files have been generated
    assert sample_dir.joinpath('CLASS-UPPER-EDGE_LS8-NDVI_clipped.tif').exists()
    assert sample_dir.joinpath('ASPECT-OCT_merged.tif').exists()
    assert sample_dir.joinpath('transects.gpkg').exists()

    points = gpd.read_file(sample_dir.joinpath('transects.gpkg'),layer='points')
    lines = gpd.read_file(sample_dir.joinpath('transects.gpkg'),layer='lines')

    # check if number of points per transect matches length parameter
    assert points.shape[0] % 9 == 0
    # check if number of transects matches number of points
    assert points.shape[0] / 9 == lines.shape[0]








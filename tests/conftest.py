import shutil
from pathlib import Path

import pytest

test_data_dir = Path(__file__).parent.resolve().joinpath('test_data')
sample_dir = test_data_dir.joinpath('ClippedSample')
unprocessed_dir = test_data_dir.joinpath('ClippedSampleUnprocessed')

@pytest.fixture
def remove_test_data_dir():
    if sample_dir.exists():
        shutil.rmtree(sample_dir)

@pytest.fixture
def create_test_data_dir():
    sample_dir.mkdir()
    for file in unprocessed_dir.glob('*'):
        shutil.copy2(file,sample_dir.joinpath(file.name))
